package com.example.assembleapparel;

import android.app.DialogFragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class FragmentRecycler extends DialogFragment implements View.OnClickListener {

    public Button addToCart, increment, decrement;
    public TextView productName, quantity;
    public RadioGroup radioGroup;
    public RadioButton small, medium, large, extraLarge;
    public String size = "", brand, type;
    public FirebaseUser firebaseUser;
    public DatabaseReference rUserDatabase;
    public FirebaseAuth mAuth;
    public Integer position;
    public static String userID;
    public SharedPreferences pref;
    public SharedPreferences.Editor editor;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recycler, container, false);
        getDialog().setTitle("Product Details");

        mAuth = FirebaseAuth.getInstance();
        FirebaseApp.initializeApp(getActivity());
        rUserDatabase = FirebaseDatabase.getInstance().getReference();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        mAuth = FirebaseAuth.getInstance();

        pref = getActivity().getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();
        userID = pref.getString("user_id", null); // getting String

        Bundle mArgs = getArguments();
        final String product = mArgs.getString("product_name");
        brand = mArgs.getString("brand");
        type = mArgs.getString("type");
        position=mArgs.getInt("position");

        addToCart=rootView.findViewById(R.id.addToCart);
        productName=rootView.findViewById(R.id.productName);
        small=rootView.findViewById(R.id.smallSize);
        medium=rootView.findViewById(R.id.mediumSize);
        large=rootView.findViewById(R.id.largeSize);
        extraLarge=rootView.findViewById(R.id.extraLargeSize);
        radioGroup=rootView.findViewById(R.id.radioGroup);
        quantity = rootView.findViewById(R.id.quantity);
        increment = rootView.findViewById(R.id.increment);
        decrement = rootView.findViewById(R.id.decrement);

        increment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int q = Integer.parseInt(quantity.getText().toString());
                if(q<6){
                    q++;
                    quantity.setText(Integer.toString(q));
                }
                else {
                    Toast.makeText(getActivity(), "Maximum quantity allowed: 6", Toast.LENGTH_SHORT).show();
                }
            }
        });

        decrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int q = Integer.parseInt(quantity.getText().toString());
                if(q>1){
                    q--;
                    quantity.setText(Integer.toString(q));
                }
                else {
                    Toast.makeText(getActivity(), "Quantity must be at least 1", Toast.LENGTH_SHORT).show();
                }
            }
        });

        productName.setText(product);

        small.setOnClickListener(this);
        medium.setOnClickListener(this);
        large.setOnClickListener(this);
        extraLarge.setOnClickListener(this);

        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(size.equals("")){
                    Toast.makeText(getActivity(), "Please Select Size", Toast.LENGTH_SHORT).show();
                }
                else {
                    addItem(Integer.parseInt(quantity.getText().toString()), size, position);
                }
                //Toast.makeText(getActivity(),
                // "Quantity: " + quantity.getText().toString() + " Size: " + size , Toast.LENGTH_SHORT).show();
            }
        });

        return rootView;
    }

    private void addItem(Integer quantity, String size, Integer position) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        Product product = new Product(brand, type, size, quantity, position);
        DatabaseReference mRef = database.getReference().child("User").child(userID).child("Products").push();
        mRef.setValue(product);
        Intent added = new Intent(getActivity(), AddedToCartActivity.class);
        startActivity(added);

    }

    @Override
    public void onClick(View v) {
        if (radioGroup.getCheckedRadioButtonId()== R.id.smallSize) {
            size="s";
        }
        if (radioGroup.getCheckedRadioButtonId() == R.id.mediumSize) {
            size="m";
        }
        if (radioGroup.getCheckedRadioButtonId() == R.id.largeSize) {
            size="l";

        }
        if (radioGroup.getCheckedRadioButtonId() == R.id.extraLargeSize) {
            size="el";

        }
        if (radioGroup.getCheckedRadioButtonId() == -1){
            size="";
        }
    }
}

