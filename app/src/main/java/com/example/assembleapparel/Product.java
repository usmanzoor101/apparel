package com.example.assembleapparel;

public class Product {
    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String size, brand, type;
    public Integer quantity, position;

    public Product(){

    }

    public Product(String brand, String type, String size, Integer quantity, Integer position){
        this.brand=brand;
        this.size=size;
        this.type=type;
        this.quantity=quantity;
        this.position=position;
    }
}
