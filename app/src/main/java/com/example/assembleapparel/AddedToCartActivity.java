package com.example.assembleapparel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AddedToCartActivity extends AppCompatActivity {
    public Button viewCart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_added_to_cart);

        viewCart = findViewById(R.id.viewCart);

        viewCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cart = new Intent(AddedToCartActivity.this, CartActivity.class);
                startActivity(cart);
                finish();
            }
        });
    }
}
