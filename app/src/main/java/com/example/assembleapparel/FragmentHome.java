package com.example.assembleapparel;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


public class FragmentHome extends DialogFragment {
    public Button shirts, trousers, hoodies;
    public TextView brandName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        getDialog().setTitle("Options");


        Bundle mArgs = getArguments();
        final String brand = mArgs.getString("brand_name");


        shirts=rootView.findViewById(R.id.shirts);
        hoodies=rootView.findViewById(R.id.hoodies);
        trousers=rootView.findViewById(R.id.trousers);
        brandName=rootView.findViewById(R.id.brandName);

        brandName.setText(brand);


       shirts.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               if(brand.equals("GOT")){
                   Intent intent = new Intent(getActivity(), GOTShirtsActivity.class);
                   intent.putExtra("Brand", brand);
                   intent.putExtra("Type", "Shirts");
                   startActivity(intent);
               }
               else if (brand.equals("Star Wars")){
                   Intent intent = new Intent(getActivity(), StarwarsShirtsActivity.class);
                   intent.putExtra("Brand", brand);
                   intent.putExtra("Type", "Shirts");
                   startActivity(intent);
               }
               else if(brand.equals("Harry Potter")){
                   Intent intent = new Intent(getActivity(), HPShirtsActivity.class);
                   intent.putExtra("Brand", brand);
                   intent.putExtra("Type", "Shirts");
                   startActivity(intent);
               }
               else if(brand.equals("DC")){
                   Intent intent = new Intent(getActivity(), DCShirtsActivity.class);
                   intent.putExtra("Brand", brand);
                   intent.putExtra("Type", "Shirts");
                   startActivity(intent);
               }
               else if(brand.equals("Marvel")){
                   Intent intent = new Intent(getActivity(), MarvelShirtsActivity.class);
                   intent.putExtra("Brand", brand);
                   intent.putExtra("Type", "Shirts");
                   startActivity(intent);
               }
               else {
                   Intent intent = new Intent(getActivity(), OthersShirtsActivity.class);
                   intent.putExtra("Brand", brand);
                   intent.putExtra("Type", "Shirts");
                   startActivity(intent);
               }

           }
       });

       hoodies.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               if(brand.equals("GOT")){
                   Intent intent = new Intent(getActivity(), GOTHoodiesActivity.class);
                   intent.putExtra("Brand", brand);
                   intent.putExtra("Type", "Hoodies");
                   startActivity(intent);
               }
               else if (brand.equals("Star Wars")){
                   Intent intent = new Intent(getActivity(), StarwarsHoodiesActivity.class);
                   intent.putExtra("Brand", brand);
                   intent.putExtra("Type", "Hoodies");
                   startActivity(intent);
               }
               else if(brand.equals("Harry Potter")){
                   Intent intent = new Intent(getActivity(), HPHoodiesActivity.class);
                   intent.putExtra("Brand", brand);
                   intent.putExtra("Type", "Hoodies");
                   startActivity(intent);
               }
               else if(brand.equals("DC")){
                   Intent intent = new Intent(getActivity(), DCHoodiesActivity.class);
                   intent.putExtra("Brand", brand);
                   intent.putExtra("Type", "Hoodies");
                   startActivity(intent);
               }
               else if(brand.equals("Marvel")){
                   Intent intent = new Intent(getActivity(), MarvelHoodiesActivity.class);
                   intent.putExtra("Brand", brand);
                   intent.putExtra("Type", "Hoodies");
                   startActivity(intent);
               }
               else {
                   Intent intent = new Intent(getActivity(), OthersHoodiesActivity.class);
                   intent.putExtra("Brand", brand);
                   intent.putExtra("Type", "Hoodies");
                   startActivity(intent);
               }


           }
       });

       trousers.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               if(brand.equals("GOT")){
                   Intent intent = new Intent(getActivity(), GOTTrousersActivity.class);
                   intent.putExtra("Brand", brand);
                   intent.putExtra("Type", "Trousers");
                   startActivity(intent);
               }
               else if (brand.equals("Star Wars")){
                   Intent intent = new Intent(getActivity(), StarwarsTrousersActivity.class);
                   intent.putExtra("Brand", brand);
                   intent.putExtra("Type", "Trousers");
                   startActivity(intent);
               }
               else if(brand.equals("Harry Potter")){
                   Intent intent = new Intent(getActivity(), HPTrousersActivity.class);
                   intent.putExtra("Brand", brand);
                   intent.putExtra("Type", "Trousers");
                   startActivity(intent);
               }
               else if(brand.equals("DC")){
                   Intent intent = new Intent(getActivity(), DCTrousersActivity.class);
                   intent.putExtra("Brand", brand);
                   intent.putExtra("Type", "Trousers");
                   startActivity(intent);
               }
               else if(brand.equals("Marvel")){
                   Intent intent = new Intent(getActivity(), MarvelTrousersActivity.class);
                   intent.putExtra("Brand", brand);
                   intent.putExtra("Type", "Trousers");
                   startActivity(intent);
               }
               else {
                   Intent intent = new Intent(getActivity(), OthersTrousersActivity.class);
                   intent.putExtra("Brand", brand);
                   intent.putExtra("Type", "Trousers");
                   startActivity(intent);
               }


           }
       });


        return rootView;
    }
}
