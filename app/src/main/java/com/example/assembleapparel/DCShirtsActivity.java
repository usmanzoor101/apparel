package com.example.assembleapparel;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class DCShirtsActivity extends AppCompatActivity {

    public static String userID;
    public RecyclerView recyclerView;
    public ArrayList<String> nameList;
    public ArrayList<Integer> priceList;
    public ArrayList<String> idList;
    public ArrayList<Integer> imageList;
    public String brand, type;


    RecyclerAdapter recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dcshirts);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        //SharedPreferences.Editor editor = pref.edit();
        userID = pref.getString("user_id", null); // getting String

        Intent get = getIntent();
        brand = get.getStringExtra("Brand");
        type = get.getStringExtra("Type");


        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        nameList = new ArrayList<>();
        priceList = new ArrayList<>();
        idList = new ArrayList<>();
        imageList = new ArrayList<>();
        setAdapter();

    }

    private void setAdapter() {
//        nameList.clear();
//        priceList.clear();
//        imageList.clear();
//        idList.clear();
//        recyclerView.removeAllViews();

        nameList.add("The Dark Knight");
        nameList.add("Superman");
        nameList.add("Superman tee");
        nameList.add("Superman tee");



        priceList.add(800);
        priceList.add(500);
        priceList.add(1000);
        priceList.add(750);



        imageList.add(R.drawable.dc_one);
        imageList.add(R.drawable.dc_two);
        imageList.add(R.drawable.dc_three);
        imageList.add(R.drawable.dc_four);



        idList.add("36");
        idList.add("37");
        idList.add("38");
        idList.add("39");






        recyclerAdapter = new RecyclerAdapter(DCShirtsActivity.this,
                nameList, priceList, imageList, idList, brand, type);
        recyclerView.setAdapter(recyclerAdapter);
    }
}
