package com.example.assembleapparel;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class OthersHoodiesActivity extends AppCompatActivity {
    public static String userID;
    public RecyclerView recyclerView;
    public ArrayList<String> nameList;
    public ArrayList<Integer> priceList;
    public ArrayList<String> idList;
    public ArrayList<Integer> imageList;

    RecyclerAdapter recyclerAdapter;

    public String brand, type;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_others_hoodies);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        //SharedPreferences.Editor editor = pref.edit();
        userID = pref.getString("user_id", null); // getting String

        Intent get = getIntent();
        brand = get.getStringExtra("Brand");
        type = get.getStringExtra("Type");


        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        nameList = new ArrayList<>();
        priceList = new ArrayList<>();
        idList = new ArrayList<>();
        imageList = new ArrayList<>();
        setAdapter();

    }

    private void setAdapter() {
//        nameList.clear();
//        priceList.clear();
//        imageList.clear();
//        idList.clear();
//        recyclerView.removeAllViews();

        nameList.add("Gym ");
        nameList.add("Under Construction");
        nameList.add("GOT");
        nameList.add("Expicit Content");
        nameList.add("Decider");



        priceList.add(1800);
        priceList.add(1550);
        priceList.add(1000);
        priceList.add(1950);
        priceList.add(1950);



        imageList.add(R.drawable.other_hoodie_one);
        imageList.add(R.drawable.other_hoodie_two);
        imageList.add(R.drawable.other_hoodie_three);
        imageList.add(R.drawable.other_hoodie_four);
        imageList.add(R.drawable.other_hoodie_five);



        idList.add("69");
        idList.add("70");
        idList.add("71");
        idList.add("72");
        idList.add("73");



        recyclerAdapter = new RecyclerAdapter(OthersHoodiesActivity.this,
                nameList, priceList, imageList, idList, brand, type);
        recyclerView.setAdapter(recyclerAdapter);
    }
}