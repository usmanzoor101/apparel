package com.example.assembleapparel;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MarvelShirtsActivity extends AppCompatActivity {
    public static String userID;
    public RecyclerView recyclerView;
    public ArrayList<String> nameList;
    public ArrayList<Integer> priceList;
    public ArrayList<String> idList;
    public ArrayList<Integer> imageList;

    public String brand, type;

    RecyclerAdapter recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marvel_shirts);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        //SharedPreferences.Editor editor = pref.edit();
        userID = pref.getString("user_id", null); // getting String

        Intent get = getIntent();
        brand = get.getStringExtra("Brand");
        type = get.getStringExtra("Type");


        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        nameList = new ArrayList<>();
        priceList = new ArrayList<>();
        idList = new ArrayList<>();
        imageList = new ArrayList<>();
        setAdapter();

    }

    private void setAdapter() {
//        nameList.clear();
//        priceList.clear();
//        imageList.clear();
//        idList.clear();
//        recyclerView.removeAllViews();

        nameList.add("Thanos");
        nameList.add("DC comics");
        nameList.add("Thor");
        nameList.add("IronMan");
        nameList.add("IronMan");
        nameList.add("Captain Marvel");




        priceList.add(1800);
        priceList.add(1500);
        priceList.add(1000);
        priceList.add(950);
        priceList.add(1000);
        priceList.add(1950);



        imageList.add(R.drawable.marvel_one);
        imageList.add(R.drawable.marvel_two);
        imageList.add(R.drawable.marvel_three);
        imageList.add(R.drawable.marvel_four);
        imageList.add(R.drawable.marvel_five);
        imageList.add(R.drawable.marvel_six);



        idList.add("45");
        idList.add("46");
        idList.add("47");
        idList.add("48");
        idList.add("49");
        idList.add("50");




        recyclerAdapter = new RecyclerAdapter(MarvelShirtsActivity.this,
                nameList, priceList, imageList, idList, brand, type);
        recyclerView.setAdapter(recyclerAdapter);
    }
}
