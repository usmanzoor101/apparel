package com.example.assembleapparel;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class GOTTrousersActivity extends AppCompatActivity {
    public static String userID;
    public RecyclerView recyclerView;
    public ArrayList<String> nameList;
    public ArrayList<Integer> priceList;
    public ArrayList<String> idList;
    public ArrayList<Integer> imageList;

    RecyclerAdapter recyclerAdapter;

    public String brand, type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gottrousers);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        //SharedPreferences.Editor editor = pref.edit();
        userID = pref.getString("user_id", null); // getting String

        Intent get = getIntent();
        brand = get.getStringExtra("Brand");
        type = get.getStringExtra("Type");

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        nameList = new ArrayList<>();
        priceList = new ArrayList<>();
        idList = new ArrayList<>();
        imageList = new ArrayList<>();
        setAdapter();

    }

    private void setAdapter() {
//        nameList.clear();
//        priceList.clear();
//        imageList.clear();
//        idList.clear();
//        recyclerView.removeAllViews();

        nameList.add("House of Targareyon");
        nameList.add("House of Baratheyon");
        nameList.add("North Remembers");
        nameList.add("Game of thrones");


        priceList.add(1500);
        priceList.add(1000);
        priceList.add(1500);
        priceList.add(1200);


        imageList.add(R.drawable.got_trousers_one);
        imageList.add(R.drawable.got_trousers_two);
        imageList.add(R.drawable.got_trousers_three);
        imageList.add(R.drawable.got_trousers_four);


        idList.add("10");
        idList.add("11");
        idList.add("12");
        idList.add("13");


        recyclerAdapter = new RecyclerAdapter(GOTTrousersActivity.this,
                nameList, priceList, imageList, idList, brand, type);
        recyclerView.setAdapter(recyclerAdapter);
    }
}
