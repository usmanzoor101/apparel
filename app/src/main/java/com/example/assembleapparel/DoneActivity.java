package com.example.assembleapparel;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class DoneActivity extends AppCompatActivity {
    public Button home;
    public FirebaseUser firebaseUser;
    public DatabaseReference rUserDatabase;
    public FirebaseAuth mAuth;
    public static String userID;
    public SharedPreferences pref;
    public SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_done);

        mAuth = FirebaseAuth.getInstance();
        FirebaseApp.initializeApp(this);
        rUserDatabase = FirebaseDatabase.getInstance().getReference();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        mAuth = FirebaseAuth.getInstance();

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();
        userID = pref.getString("user_id", null); // getting String





        home = findViewById(R.id.home);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(DoneActivity.this, HomeActivity.class);
                startActivity(home);
                finish();
            }
        });

        emptyCart();
    }

    private void emptyCart() {
        rUserDatabase= FirebaseDatabase.getInstance().getReference("User").child(userID);
        rUserDatabase.child("Products").removeValue();
    }
    }

