package com.example.assembleapparel;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class RegisterActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    public Button registerButton;
    public String getFName,getLName,getEmail,getPassword,getGender,getPhone, getAddress, getAge;
    public EditText rFirstName,rLastName,rAge,rEmail,rAddress,rPhoneNumber,rPassword;
    public Spinner rGender;
    public String checkEntries;
    public boolean emailValid = true, empty = false;

    public FirebaseUser firebaseUser;
    public DatabaseReference rUserDatabase;
    public FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();
        FirebaseApp.initializeApp(this);
        rUserDatabase = FirebaseDatabase.getInstance().getReference();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        mAuth = FirebaseAuth.getInstance();

        //initialize

        registerButton = (Button) findViewById(R.id.registerButton);
        rFirstName = (EditText) findViewById(R.id.rFirstName);
        rLastName = (EditText) findViewById(R.id.rLastName);
        rAge = (EditText) findViewById(R.id.rAge);
        rEmail = (EditText) findViewById(R.id.rEmail);
        rAddress = (EditText) findViewById(R.id.rAddress);
        rPhoneNumber = (EditText) findViewById(R.id.rPhoneNumber);
        rPassword = (EditText) findViewById(R.id.rPassword);


        // Spinner element
        rGender = (Spinner) findViewById(R.id.rGender);

        // Spinner click listener
        rGender.setOnItemSelectedListener(RegisterActivity.this);

        //spinner two
        List<String> gender = new ArrayList<String>();
        gender.add("Male");
        gender.add("Female");

        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, gender);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        rGender.setAdapter(dataAdapter1);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(RegisterActivity.this, "Clicked", Toast.LENGTH_SHORT).show();

                checkEntries = "";
                emailValid = true;
                empty = false;

                getFName = rFirstName.getText().toString();
                getLName = rLastName.getText().toString();
                getAge = rAge.getText().toString();
                getEmail = rEmail.getText().toString();
                getPassword = rPassword.getText().toString();
                getPhone = rPhoneNumber.getText().toString();
                getAddress = rAddress.getText().toString();

                if ( (TextUtils.isEmpty(getFName))
                        || (TextUtils.isEmpty(getLName))
                        || (TextUtils.isEmpty(getEmail))
                        || (TextUtils.isEmpty(getPassword))
                        || (TextUtils.isEmpty(getPhone))
                        || (TextUtils.isEmpty(getAddress))
                        || (TextUtils.isEmpty(getGender))
                        || (TextUtils.isEmpty(getAge))

                ) {
                    empty=true;
                }

                if(!empty) {

                    if (
                            validateEmail(getEmail)) {

                        registerUser(getEmail);

                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                        builder.setMessage("" + checkEntries).setTitle("Error");
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
                else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                    builder.setMessage("Please enter ALL fields").setTitle("Error");
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }

            }
        });
    }

    private void registerUser(final String getEmail) {
        rUserDatabase.child("User").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                    String uId = snapshot.getKey();
                    String uEmail = snapshot.child("userEmail").getValue(String.class);



                    if(uEmail.toLowerCase().equals(getEmail.toLowerCase())){
                        emailValid = false;
                        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                        builder.setMessage("Email already exists!").setTitle("Error");
                        AlertDialog dialog = builder.create();
                        dialog.show();

                    }
                    if(emailValid){
                        writeNewUser(getFName, getLName, getEmail, getPhone, getAddress, getPassword, getGender,
                                getAge);

                        Intent registered = new Intent(RegisterActivity.this, LoginActivity.class);
                        startActivity(registered);
                        finish();
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void writeNewUser(String getFName, String getLName, String getEmail, String getPhone, String getAddress, String getPassword, String getGender, String getAge) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        Log.d("firebase", database.toString());
        User user = new User(getFName, getLName, getEmail, getPhone, getAddress, getPassword, getGender, getAge,0);
        DatabaseReference mRef = database.getReference().child("User").push(); //push creates id
        Log.d("firebase", mRef.toString());
        mRef.setValue(user);
    }

    private boolean validateEmail(String getEmail) {
        checkEntries = "";
        boolean valid = true;
        if(!Patterns.EMAIL_ADDRESS.matcher(getEmail).matches()) {
            valid = false;
            checkEntries = checkEntries + "Invalid Email \n";
        }
        return valid;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        getGender = parent.getItemAtPosition(position).toString();


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
