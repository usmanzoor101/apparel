package com.example.assembleapparel;

import android.content.Intent;
import android.content.SharedPreferences;
import android.se.omapi.Session;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {
    // UI references.
    public Button login,register;
    public static String userID;
    public EditText email;
    public EditText password;
    public DatabaseReference lUserDatabase;
    public FirebaseUser firebaseLoginUser;
    public Boolean checkValidity =true;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        email = (EditText)findViewById(R.id.email);
        password = (EditText)findViewById(R.id.password);
        login = (Button)findViewById(R.id.loginButton);

        FirebaseApp.initializeApp(this);
        lUserDatabase = FirebaseDatabase.getInstance().getReference();
        firebaseLoginUser = FirebaseAuth.getInstance().getCurrentUser();

//        Used register to make use of firebase
        register=(Button)findViewById(R.id.registerButton);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userEmail = email.getText().toString();
                String userPassword = password.getText().toString();
                if(TextUtils.isEmpty(userEmail) || TextUtils.isEmpty(userPassword)){
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setMessage("Please enter all fields").setTitle("Error");
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                else {
                    isValid(userEmail, userPassword);
                }

            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerPage= new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(registerPage);

            }
        });

    }

    private void isValid(final String emailEntered, final String passwordEntered) {
        lUserDatabase.child("User").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                    String uId = snapshot.getKey();
                    String uEmail = snapshot.child("userEmail").getValue(String.class);
                    String uPassword = snapshot.child("userPassword").getValue(String.class);


                    //If email password matches, return true
                    if(uEmail!= null && uPassword != null) {
                        if (uEmail.toLowerCase().equals(emailEntered.toLowerCase()) && uPassword.equals(passwordEntered)
                        )
                        {
                            checkValidity = false;
                            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("user_id", uId); // Storing string
                            editor.putString("user_email", uEmail); // Storing string
                            editor.commit(); // commit changes
                            Intent mainPage= new Intent(LoginActivity.this,HomeActivity.class);
                            startActivity(mainPage);
                            finish();

                        }
                    }
                }
                if(checkValidity){
                    //error of invalid details for login
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setMessage("Invalid Username or Password").setTitle("Error");
                    AlertDialog dialog = builder.create();
                    dialog.show();



                }
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("onCancelled", "cancelled");

            }
        });
    }
}
