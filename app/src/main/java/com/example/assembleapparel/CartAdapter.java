package com.example.assembleapparel;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.SearchViewHolder>  {
    public Context context;

    public ArrayList<String> sizeList;
    public ArrayList<Integer> quantityList;
    public ArrayList<String> nameList;
    public ArrayList<Integer> priceList;
    public ArrayList<Integer> imageList;
    public ArrayList<String> idList;

    @Override
    public SearchViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cart_list, parent, false);
        return new CartAdapter.SearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchViewHolder holder, final int position) {
        holder.vName.setText(nameList.get(position));
        holder.vPrice.setText(priceList.get(position).toString());
        holder.vImage.setImageResource(imageList.get(position));
        holder.vSize.setText(sizeList.get(position));
        holder.vQuantity.setText(quantityList.get(position).toString());
        int total = priceList.get(position) * quantityList.get(position);
        holder.vSubTotal.setText(Integer.toString(total));

    }

    @Override
    public int getItemCount() {
        return idList.size();
    }

    public class SearchViewHolder extends RecyclerView.ViewHolder {
        TextView vName, vPrice, vSize, vQuantity, vSubTotal;
        ImageView vImage;

        //binding textviews to layout xml
        public SearchViewHolder(View itemView) {
            super(itemView);
            vName = itemView.findViewById(R.id.cartName);
            vPrice = itemView.findViewById(R.id.cartPrice);
            vImage = itemView.findViewById(R.id.cartImage);
            vSize = itemView.findViewById(R.id.cartSize);
            vQuantity = itemView.findViewById(R.id.cartQuantity);
            vSubTotal = itemView.findViewById(R.id.cartSubTotal);

        }
    }

    public CartAdapter(Context context,
                       ArrayList<String> sizeList,
                       ArrayList<Integer> quantityList,
                       ArrayList<String> nameList,  ArrayList<Integer> priceList,
                           ArrayList<Integer> imageList, ArrayList<String> idList){
        this.context = context;
        this.nameList = nameList;
        this.priceList = priceList;
        this.imageList = imageList;
        this.idList = idList;

        this.quantityList=quantityList;
        this.sizeList=sizeList;

    }

}
