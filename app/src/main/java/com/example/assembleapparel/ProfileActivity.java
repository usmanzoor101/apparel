package com.example.assembleapparel;

import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class ProfileActivity extends AppCompatActivity {
    //for firebase references
    private FirebaseAuth mAuth;
    public static String userID;
    public DatabaseReference updateUserDatabase;
    public FirebaseUser firebaseProfileUser;
    public DatabaseReference pUserDatabase;

    public TextView profileFirstName, profileLastName, profilePhoneNumber, profileAddress, profileAge, profileGender;
    public TextView profileEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        //SharedPreferences.Editor editor = pref.edit();
        userID = pref.getString("user_id", null); // getting String

        //firebase initialize
        FirebaseApp.initializeApp(this);
        pUserDatabase = FirebaseDatabase.getInstance().getReference();
        firebaseProfileUser = FirebaseAuth.getInstance().getCurrentUser();
        mAuth = FirebaseAuth.getInstance();


        //initialize
        profileFirstName = findViewById(R.id.profileFirstName);
        profileLastName = findViewById(R.id.profileLastName);
        profileAge = findViewById(R.id.profileAge);
        profileAddress = findViewById(R.id.profileAddress);
        profileEmail = findViewById(R.id.profileEmail);
        profileGender = findViewById(R.id.profileGender);
        profilePhoneNumber = findViewById(R.id.profilePhoneNumber);


        pUserDatabase = FirebaseDatabase.getInstance().getReference();
        firebaseProfileUser = FirebaseAuth.getInstance().getCurrentUser();

        getProfile(userID);

    }

    public void getProfile(final String userID) {

        pUserDatabase.child("User").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    String pId = snapshot.getKey();


                    if (userID.equals(pId)) {

                        Log.d("checkID", pId);
                        String pFName = snapshot.child("userFname").getValue(String.class);
                        String pLName = snapshot.child("userLname").getValue(String.class);
                        String pEmail = snapshot.child("userEmail").getValue(String.class);
                        String pPhoneNumber = snapshot.child("userPhone").getValue(String.class);
                        String pAddress = snapshot.child("userAddress").getValue(String.class);
                        String pGender = snapshot.child("userGender").getValue(String.class);
                        String pAge = snapshot.child("userAge").getValue(String.class);

                        profileFirstName.setText(pFName);
                        profileLastName.setText(pLName);
                        profileEmail.setText(pEmail);
                        profilePhoneNumber.setText(pPhoneNumber);
                        profileAddress.setText(pAddress);
                        profileAge.setText(pAge);
                        profileGender.setText(pGender);



                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}