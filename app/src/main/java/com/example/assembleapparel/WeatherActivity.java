package com.example.assembleapparel;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class WeatherActivity extends AppCompatActivity {

    TextView temp, pressure, humidity, cityName;

    /* Please Put your API KEY here */
    String OPEN_WEATHER_MAP_API = "c10bb3bd22f90d636baa008b1529ee25";
    /* Please Put your API KEY here */
    public String url =
            "https://api.openweathermap.org/data/2.5/weather?q=lahore&units=metric&APPID=c10bb3bd22f90d636baa008b1529ee25";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        temp = findViewById(R.id.temp);
        pressure = (TextView) findViewById(R.id.pressure);
        humidity = (TextView) findViewById(R.id.humidity);
        cityName = findViewById(R.id.cityName);

        getResponse();

    }

    private void getResponse() {
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String name = jsonObject.getString("name");
                    cityName.setText(name);
                    JSONObject main = jsonObject.getJSONObject("main");
                    Double temperature = main.getDouble("temp");
                    temp.setText(Double.toString(temperature));
                    Integer pressure_get = main.getInt("pressure");
                    pressure.setText(Integer.toString(pressure_get));
                    Integer humidity_get = main.getInt("humidity");
                    humidity.setText(Double.toString(humidity_get));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
//                humidity.setText(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(WeatherActivity.this, error.toString(), Toast.LENGTH_SHORT).show();

            }
        });
        queue.add(stringRequest);


    }


}


