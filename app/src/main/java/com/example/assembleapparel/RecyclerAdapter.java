package com.example.assembleapparel;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.SearchViewHolder>  {
    public Context context;
    public ArrayList<String> nameList;
    public ArrayList<Integer> priceList;
    public ArrayList<Integer> imageList;
    public ArrayList<String> idList;
    public String brand, type;

    public class SearchViewHolder extends RecyclerView.ViewHolder {
        TextView vName, vPrice;
        ImageView vImage;
        Button cartButton;

        //binding textviews to layout xml
        public SearchViewHolder(View itemView) {
            super(itemView);
            vName = itemView.findViewById(R.id.showName);
            vPrice = itemView.findViewById(R.id.showPrice);
            vImage = itemView.findViewById(R.id.showImage);
            cartButton = itemView.findViewById(R.id.cartButton);

        }
    }

    public RecyclerAdapter(Context context, ArrayList<String> nameList,  ArrayList<Integer> priceList,
                           ArrayList<Integer> imageList, ArrayList<String> idList, String brand, String type){
        this.context = context;
        this.nameList = nameList;
        this.priceList = priceList;
        this.imageList = imageList;
        this.idList = idList;
        this.brand=brand;
        this.type=type;
    }


    @Override
    public RecyclerAdapter.SearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_list, parent, false);
        return new RecyclerAdapter.SearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchViewHolder holder, final int position) {
        holder.vName.setText(nameList.get(position));
        holder.vPrice.setText(priceList.get(position).toString());
        holder.vImage.setImageResource(imageList.get(position));

        holder.cartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putString("product_name", nameList.get(position));
                args.putString("brand", brand);
                args.putString("type", type);
                args.putInt("position", position);
                FragmentManager fm = ((Activity)context).getFragmentManager();
                FragmentRecycler fragmentRecycler= new FragmentRecycler();
                fragmentRecycler.setArguments(args);
                fragmentRecycler.show(fm, "Sample Fragment");
            }
        });


    }

    @Override
    public int getItemCount() {
        return idList.size();
    }
}
