package com.example.assembleapparel;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class CartActivity extends AppCompatActivity {
    public static String userID;
    public SharedPreferences pref;
    public SharedPreferences.Editor editor;
    public RecyclerView recyclerView;
    public TextView totalPrice;
    public Integer total = 0;
    public Button checkout;
    public String TotalSend;
    public Integer items;
    CartAdapter cartAdapter;


    public ArrayList<String> nameList = new ArrayList<>();
    public ArrayList<Integer> priceList= new ArrayList<>();
    public ArrayList<String> idList= new ArrayList<>();
    public ArrayList<Integer> imageList= new ArrayList<>();
//    public ArrayList<String> brandList;
//    public ArrayList<String> typeList;
    public ArrayList<String> sizeList= new ArrayList<>();
    public ArrayList<Integer> quantityList= new ArrayList<>();
//    public ArrayList<Integer> positionList;


    public ArrayList<String> DCHoodiesName= new ArrayList<>();
    public ArrayList<Integer> DCHoodiesPrice= new ArrayList<>();
    public ArrayList<String> DCHoodiesId= new ArrayList<>();
    public ArrayList<Integer> DCHoodiesImages= new ArrayList<>();
    public ArrayList<String> DCShirtsName= new ArrayList<>();
    public ArrayList<Integer> DCShirtsPrice= new ArrayList<>();
    public ArrayList<String> DCShirtsId= new ArrayList<>();
    public ArrayList<Integer> DCShirtsImages= new ArrayList<>();
    public ArrayList<String> DCTrousersName= new ArrayList<>();
    public ArrayList<Integer> DCTrousersPrice= new ArrayList<>();
    public ArrayList<String> DCTrousersId= new ArrayList<>();
    public ArrayList<Integer> DCTrousersImages= new ArrayList<>();
    public FirebaseUser firebaseUser;
    public DatabaseReference rUserDatabase;
    public FirebaseAuth mAuth;


    public ArrayList<String> GOTHoodiesName= new ArrayList<>();
    public ArrayList<Integer> GOTHoodiesPrice= new ArrayList<>();
    public ArrayList<String> GOTHoodiesId= new ArrayList<>();
    public ArrayList<Integer> GOTHoodiesImages= new ArrayList<>();
    public ArrayList<String> GOTShirtsName= new ArrayList<>();
    public ArrayList<Integer> GOTShirtsPrice= new ArrayList<>();
    public ArrayList<String> GOTShirtsId= new ArrayList<>();
    public ArrayList<Integer> GOTShirtsImages= new ArrayList<>();
    public ArrayList<String> GOTTrousersName= new ArrayList<>();
    public ArrayList<Integer> GOTTrousersPrice= new ArrayList<>();
    public ArrayList<String> GOTTrousersId= new ArrayList<>();
    public ArrayList<Integer> GOTTrousersImages= new ArrayList<>();

    public ArrayList<String> HPHoodiesName= new ArrayList<>();
    public ArrayList<Integer> HPHoodiesPrice= new ArrayList<>();
    public ArrayList<String> HPHoodiesId= new ArrayList<>();
    public ArrayList<Integer> HPHoodiesImages= new ArrayList<>();
    public ArrayList<String> HPShirtsName= new ArrayList<>();
    public ArrayList<Integer> HPShirtsPrice= new ArrayList<>();
    public ArrayList<String> HPShirtsId= new ArrayList<>();
    public ArrayList<Integer> HPShirtsImages= new ArrayList<>();
    public ArrayList<String> HPTrousersName= new ArrayList<>();
    public ArrayList<Integer> HPTrousersPrice= new ArrayList<>();
    public ArrayList<String> HPTrousersId= new ArrayList<>();
    public ArrayList<Integer> HPTrousersImages= new ArrayList<>();


    public ArrayList<String> MarvelHoodiesName= new ArrayList<>();
    public ArrayList<Integer> MarvelHoodiesPrice= new ArrayList<>();
    public ArrayList<String> MarvelHoodiesId= new ArrayList<>();
    public ArrayList<Integer> MarvelHoodiesImages= new ArrayList<>();
    public ArrayList<String> MarvelShirtsName= new ArrayList<>();
    public ArrayList<Integer> MarvelShirtsPrice= new ArrayList<>();
    public ArrayList<String> MarvelShirtsId= new ArrayList<>();
    public ArrayList<Integer> MarvelShirtsImages= new ArrayList<>();
    public ArrayList<String> MarvelTrousersName= new ArrayList<>();
    public ArrayList<Integer> MarvelTrousersPrice= new ArrayList<>();
    public ArrayList<String> MarvelTrousersId= new ArrayList<>();
    public ArrayList<Integer> MarvelTrousersImages= new ArrayList<>();


    public ArrayList<String> OthersHoodiesName= new ArrayList<>();
    public ArrayList<Integer> OthersHoodiesPrice= new ArrayList<>();
    public ArrayList<String> OthersHoodiesId= new ArrayList<>();
    public ArrayList<Integer> OthersHoodiesImages= new ArrayList<>();
    public ArrayList<String> OthersShirtsName= new ArrayList<>();
    public ArrayList<Integer> OthersShirtsPrice= new ArrayList<>();
    public ArrayList<String> OthersShirtsId= new ArrayList<>();
    public ArrayList<Integer> OthersShirtsImages= new ArrayList<>();
    public ArrayList<String> OthersTrousersName= new ArrayList<>();
    public ArrayList<Integer> OthersTrousersPrice= new ArrayList<>();
    public ArrayList<String> OthersTrousersId= new ArrayList<>();
    public ArrayList<Integer> OthersTrousersImages= new ArrayList<>();


    public ArrayList<String> StarwarsHoodiesName= new ArrayList<>();
    public ArrayList<Integer> StarwarsHoodiesPrice= new ArrayList<>();
    public ArrayList<String> StarwarsHoodiesId= new ArrayList<>();
    public ArrayList<Integer> StarwarsHoodiesImages= new ArrayList<>();
    public ArrayList<String> StarwarsShirtsName= new ArrayList<>();
    public ArrayList<Integer> StarwarsShirtsPrice= new ArrayList<>();
    public ArrayList<String> StarwarsShirtsId= new ArrayList<>();
    public ArrayList<Integer> StarwarsShirtsImages= new ArrayList<>();
    public ArrayList<String> StarwarsTrousersName= new ArrayList<>();
    public ArrayList<Integer> StarwarsTrousersPrice= new ArrayList<>();
    public ArrayList<String> StarwarsTrousersId= new ArrayList<>();
    public ArrayList<Integer> StarwarsTrousersImages= new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();
        userID = pref.getString("user_id", null); // getting String

        mAuth = FirebaseAuth.getInstance();
        FirebaseApp.initializeApp(this);
        rUserDatabase = FirebaseDatabase.getInstance().getReference();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        mAuth = FirebaseAuth.getInstance();


        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        totalPrice = findViewById(R.id.totalPrice);
        checkout=findViewById(R.id.checkout);

        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent pay = new Intent(CartActivity.this, PaymentActivity.class);
            pay.putExtra("TotalSend",TotalSend);
            pay.putExtra("items",items.toString());
            startActivity(pay);
            finish();
            }
        });


        DCHoodiesName.add("The Batman");
        DCHoodiesName.add("Flash");
        DCHoodiesName.add("The DC COMICS");
        DCHoodiesName.add("Flash");
        DCHoodiesPrice.add(1800);
        DCHoodiesPrice.add(1500);
        DCHoodiesPrice.add(1000);
        DCHoodiesPrice.add(950);
        DCHoodiesImages.add(R.drawable.dc_hoodie_one);
        DCHoodiesImages.add(R.drawable.dc_hoodie_two);
        DCHoodiesImages.add(R.drawable.dc_hoodie_three);
        DCHoodiesImages.add(R.drawable.dc_hoodie_four);
        DCHoodiesId.add("42");
        DCHoodiesId.add("43");
        DCHoodiesId.add("44");
        DCHoodiesId.add("44");

        DCShirtsName.add("The Dark Knight");
        DCShirtsName.add("Superman");
        DCShirtsName.add("Superman tee");
        DCShirtsName.add("Superman tee");
        DCShirtsPrice.add(800);
        DCShirtsPrice.add(500);
        DCShirtsPrice.add(1000);
        DCShirtsPrice.add(750);
        DCShirtsImages.add(R.drawable.dc_one);
        DCShirtsImages.add(R.drawable.dc_two);
        DCShirtsImages.add(R.drawable.dc_three);
        DCShirtsImages.add(R.drawable.dc_four);
        DCShirtsId.add("36");
        DCShirtsId.add("37");
        DCShirtsId.add("38");
        DCShirtsId.add("39");

        DCTrousersName.add("The DC COMICS");
        DCTrousersName.add("The Flash");
        DCTrousersPrice.add(800);
        DCTrousersPrice.add(500);
        DCTrousersImages.add(R.drawable.dc_trousers_one);
        DCTrousersImages.add(R.drawable.dc_trousers_two);
        DCTrousersId.add("40");
        DCTrousersId.add("41");

        GOTHoodiesName.add("House of Starks");
        GOTHoodiesName.add("House of Targareyan");
        GOTHoodiesName.add("North Remembers");
        GOTHoodiesName.add("Game of thrones");
        GOTHoodiesPrice.add(1500);
        GOTHoodiesPrice.add(1000);
        GOTHoodiesPrice.add(1500);
        GOTHoodiesPrice.add(1200);
        GOTHoodiesImages.add(R.drawable.got_hoodies_one);
        GOTHoodiesImages.add(R.drawable.got_hoodies_two);
        GOTHoodiesImages.add(R.drawable.got_hoodies_three);
        GOTHoodiesImages.add(R.drawable.got_hoodies_four);
        GOTHoodiesId.add("6");
        GOTHoodiesId.add("7");
        GOTHoodiesId.add("8");
        GOTHoodiesId.add("9");

        GOTShirtsName.add("King in the North");
        GOTShirtsName.add("House of Lannister");
        GOTShirtsName.add("Game of Thrones");
        GOTShirtsName.add("House of Targareyan");
        GOTShirtsName.add("Night King");
        GOTShirtsPrice.add(1100);
        GOTShirtsPrice.add(500);
        GOTShirtsPrice.add(500);
        GOTShirtsPrice.add(500);
        GOTShirtsPrice.add(500);
        GOTShirtsImages.add(R.drawable.got_one);
        GOTShirtsImages.add(R.drawable.got_two);
        GOTShirtsImages.add(R.drawable.got_three);
        GOTShirtsImages.add(R.drawable.got_four);
        GOTShirtsImages.add(R.drawable.got_five);
        GOTShirtsId.add("1");
        GOTShirtsId.add("2");
        GOTShirtsId.add("3");
        GOTShirtsId.add("4");
        GOTShirtsId.add("5");
        GOTTrousersName.add("House of Targareyon");
        GOTTrousersName.add("House of Baratheyon");
        GOTTrousersName.add("North Remembers");
        GOTTrousersName.add("Game of thrones");
        GOTTrousersPrice.add(1500);
        GOTTrousersPrice.add(1000);
        GOTTrousersPrice.add(1500);
        GOTTrousersPrice.add(1200);
        GOTTrousersImages.add(R.drawable.got_trousers_one);
        GOTTrousersImages.add(R.drawable.got_trousers_two);
        GOTTrousersImages.add(R.drawable.got_trousers_three);
        GOTTrousersImages.add(R.drawable.got_trousers_four);
        GOTTrousersId.add("10");
        GOTTrousersId.add("11");
        GOTTrousersId.add("12");
        GOTTrousersId.add("13");

        HPHoodiesName.add("Slytherin House");
        HPHoodiesName.add("Harry Potter");
        HPHoodiesName.add("Griffindor");
        HPHoodiesPrice.add(1100);
        HPHoodiesPrice.add(1500);
        HPHoodiesPrice.add(1000);
        HPHoodiesImages.add(R.drawable.hp_hoodie_one);
        HPHoodiesImages.add(R.drawable.hp_hoodie_two);
        HPHoodiesImages.add(R.drawable.hp_hoodie_three);
        HPHoodiesId.add("33");
        HPHoodiesId.add("34");
        HPHoodiesId.add("35");

        HPShirtsName.add("Harry Potter");
        HPShirtsName.add("Harry");
        HPShirtsName.add("Griffindor");
        HPShirtsName.add("Harry Potter");
        HPShirtsPrice.add(1100);
        HPShirtsPrice.add(500);
        HPShirtsPrice.add(500);
        HPShirtsPrice.add(500);
        HPShirtsImages.add(R.drawable.hp_one);
        HPShirtsImages.add(R.drawable.hp_two);
        HPShirtsImages.add(R.drawable.hp_three);
        HPShirtsImages.add(R.drawable.hp_four);
        HPShirtsId.add("27");
        HPShirtsId.add("28");
        HPShirtsId.add("29");
        HPShirtsId.add("30");
        HPTrousersName.add("Slytherin House");
        HPTrousersName.add("Harry");
        HPTrousersPrice.add(1100);
        HPTrousersPrice.add(1500);
        HPTrousersImages.add(R.drawable.hp_trousers_one);
        HPTrousersImages.add(R.drawable.hp_trousers_two);
        HPTrousersId.add("31");
        HPTrousersId.add("32");

        MarvelHoodiesName.add("Avengers");
        MarvelHoodiesName.add("Captain America");
        MarvelHoodiesName.add("The Punsiher");
        MarvelHoodiesName.add("Venom");
        MarvelHoodiesPrice.add(1800);
        MarvelHoodiesPrice.add(1500);
        MarvelHoodiesPrice.add(1000);
        MarvelHoodiesPrice.add(950);
        MarvelHoodiesImages.add(R.drawable.marvel_hoodie_one);
        MarvelHoodiesImages.add(R.drawable.marvel_hoodie_two);
        MarvelHoodiesImages.add(R.drawable.marvel_hoodie_three);
        MarvelHoodiesImages.add(R.drawable.marvel_hoodie_four);
        MarvelHoodiesId.add("55");
        MarvelHoodiesId.add("56");
        MarvelHoodiesId.add("57");
        MarvelHoodiesId.add("58");
        MarvelShirtsName.add("Thanos");
        MarvelShirtsName.add("DC comics");
        MarvelShirtsName.add("Thor");
        MarvelShirtsName.add("IronMan");
        MarvelShirtsName.add("IronMan");
        MarvelShirtsName.add("Captain Marvel");




        MarvelShirtsPrice.add(1800);
        MarvelShirtsPrice.add(1500);
        MarvelShirtsPrice.add(1000);
        MarvelShirtsPrice.add(950);
        MarvelShirtsPrice.add(1000);
        MarvelShirtsPrice.add(1950);



        MarvelShirtsImages.add(R.drawable.marvel_one);
        MarvelShirtsImages.add(R.drawable.marvel_two);
        MarvelShirtsImages.add(R.drawable.marvel_three);
        MarvelShirtsImages.add(R.drawable.marvel_four);
        MarvelShirtsImages.add(R.drawable.marvel_five);
        MarvelShirtsImages.add(R.drawable.marvel_six);



        MarvelShirtsId.add("45");
        MarvelShirtsId.add("46");
        MarvelShirtsId.add("47");
        MarvelShirtsId.add("48");
        MarvelShirtsId.add("49");
        MarvelShirtsId.add("50");
        MarvelTrousersName.add("Captain Marvel");
        MarvelTrousersName.add("DeadPool");
        MarvelTrousersName.add("GrooT");
        MarvelTrousersName.add("Avengers");





        MarvelTrousersPrice.add(1800);
        MarvelTrousersPrice.add(1500);
        MarvelTrousersPrice.add(1000);
        MarvelTrousersPrice.add(950);




        MarvelTrousersImages.add(R.drawable.marvel_trouser_one);
        MarvelTrousersImages.add(R.drawable.marvel_trouser_two);
        MarvelTrousersImages.add(R.drawable.marvel_trouser_three);
        MarvelTrousersImages.add(R.drawable.marvel_trouser_four);




        MarvelTrousersId.add("51");
        MarvelTrousersId.add("52");
        MarvelTrousersId.add("53");
        MarvelTrousersId.add("54");
        OthersHoodiesName.add("Gym ");
        OthersHoodiesName.add("Under Construction");
        OthersHoodiesName.add("GOT");
        OthersHoodiesName.add("Expicit Content");
        OthersHoodiesName.add("Decider");



        OthersHoodiesPrice.add(1800);
        OthersHoodiesPrice.add(1550);
        OthersHoodiesPrice.add(1000);
        OthersHoodiesPrice.add(1950);
        OthersHoodiesPrice.add(1950);



        OthersHoodiesImages.add(R.drawable.other_hoodie_one);
        OthersHoodiesImages.add(R.drawable.other_hoodie_two);
        OthersHoodiesImages.add(R.drawable.other_hoodie_three);
        OthersHoodiesImages.add(R.drawable.other_hoodie_four);
        OthersHoodiesImages.add(R.drawable.other_hoodie_five);



        OthersHoodiesId.add("69");
        OthersHoodiesId.add("70");
        OthersHoodiesId.add("71");
        OthersHoodiesId.add("72");
        OthersHoodiesId.add("73");
        OthersShirtsName.add("Gym Tee");
        OthersShirtsName.add("Skull");
        OthersShirtsName.add("Mojo JOJO");
        OthersShirtsName.add("Ninja Turtles");
        OthersShirtsName.add("mojo JOJO");
        OthersShirtsName.add("Design");





        OthersShirtsPrice.add(800);
        OthersShirtsPrice.add(500);
        OthersShirtsPrice.add(1000);
        OthersShirtsPrice.add(950);
        OthersShirtsPrice.add(1000);
        OthersShirtsPrice.add(350);





        OthersShirtsImages.add(R.drawable.other_one);
        OthersShirtsImages.add(R.drawable.other_two);
        OthersShirtsImages.add(R.drawable.other_three);
        OthersShirtsImages.add(R.drawable.other_four);
        OthersShirtsImages.add(R.drawable.other_five);
        OthersShirtsImages.add(R.drawable.other_six);





        OthersShirtsId.add("59");
        OthersShirtsId.add("60");
        OthersShirtsId.add("61");
        OthersShirtsId.add("62");
        OthersShirtsId.add("63");
        OthersShirtsId.add("64");
        OthersTrousersName.add("Grey");
        OthersTrousersName.add("White");
        OthersTrousersName.add("Black");
        OthersTrousersName.add("Off-white");



        OthersTrousersPrice.add(1800);
        OthersTrousersPrice.add(1550);
        OthersTrousersPrice.add(1000);
        OthersTrousersPrice.add(950);



        OthersTrousersImages.add(R.drawable.other_trousers_one);
        OthersTrousersImages.add(R.drawable.other_trousers_two);
        OthersTrousersImages.add(R.drawable.other_trousers_three);
        OthersTrousersImages.add(R.drawable.other_trousers_four);



        OthersTrousersId.add("65");
        OthersTrousersId.add("66");
        OthersTrousersId.add("67");
        OthersTrousersId.add("68");

        StarwarsHoodiesName.add("Star Wars");
        StarwarsHoodiesName.add("Star Wars");
        StarwarsHoodiesName.add("Darth Vader");
        StarwarsHoodiesName.add("Star Wars");



        StarwarsHoodiesPrice.add(1200);
        StarwarsHoodiesPrice.add(1000);
        StarwarsHoodiesPrice.add(1550);
        StarwarsHoodiesPrice.add(1500);




        StarwarsHoodiesImages.add(R.drawable.starwars_hoodies_one);
        StarwarsHoodiesImages.add(R.drawable.starwars_hoodies_two);
        StarwarsHoodiesImages.add(R.drawable.starwars_hoodies_three);
        StarwarsHoodiesImages.add(R.drawable.starwars_hoodies_four);



        StarwarsHoodiesId.add("23");
        StarwarsHoodiesId.add("24");
        StarwarsHoodiesId.add("25");
        StarwarsHoodiesId.add("26");

        StarwarsShirtsName.add("Star Wars");
        StarwarsShirtsName.add("Star Wars");
        StarwarsShirtsName.add("Darth Vader");
        StarwarsShirtsName.add("Star Wars");
        StarwarsShirtsName.add("Darth Vader");


        StarwarsShirtsPrice.add(500);
        StarwarsShirtsPrice.add(1000);
        StarwarsShirtsPrice.add(7500);
        StarwarsShirtsPrice.add(500);
        StarwarsShirtsPrice.add(750);



        StarwarsShirtsImages.add(R.drawable.starwars_one);
        StarwarsShirtsImages.add(R.drawable.starwars_two);
        StarwarsShirtsImages.add(R.drawable.starwars_three);
        StarwarsShirtsImages.add(R.drawable.starwars_four);
        StarwarsShirtsImages.add(R.drawable.starwars_five);


        StarwarsShirtsId.add("14");
        StarwarsShirtsId.add("15");
        StarwarsShirtsId.add("16");
        StarwarsShirtsId.add("17");
        StarwarsShirtsId.add("18");

        StarwarsTrousersName.add("Darth Vader");
        StarwarsTrousersName.add("Star Wars");
        StarwarsTrousersName.add("Darth Vader");
        StarwarsTrousersName.add("Star Wars");



        StarwarsTrousersPrice.add(1200);
        StarwarsTrousersPrice.add(1000);
        StarwarsTrousersPrice.add(750);
        StarwarsTrousersPrice.add(1500);




        StarwarsTrousersImages.add(R.drawable.starwars_trousers_one);
        StarwarsTrousersImages.add(R.drawable.starwars_trousers_two);
        StarwarsTrousersImages.add(R.drawable.starwars_trousers_three);
        StarwarsTrousersImages.add(R.drawable.starwars_trousers_four);
        StarwarsTrousersId.add("19");
        StarwarsTrousersId.add("20");
        StarwarsTrousersId.add("21");
        StarwarsTrousersId.add("22");

        setAdapter();
    }

    private void setAdapter() {
        rUserDatabase.child("User").child(userID).child("Products").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int i = 0;
                for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                    String uId = snapshot.getKey();
                    String pBrand = snapshot.child("brand").getValue(String.class);
                    String pSize = snapshot.child("size").getValue(String.class);
                    String pType = snapshot.child("type").getValue(String.class);
                    Integer pPosition = snapshot.child("position").getValue(Integer.class);
                    Integer pQuantity = snapshot.child("quantity").getValue(Integer.class);

                    sizeList.add(pSize);
                    quantityList.add(pQuantity);
                    if(pBrand.equals("GOT")){
                        if(pType.equals("Shirts")){
                            nameList.add(GOTShirtsName.get(pPosition));
                            priceList.add(GOTShirtsPrice.get(pPosition));
                            imageList.add(GOTShirtsImages.get(pPosition));
                            idList.add(GOTShirtsId.get(pPosition));
                        }
                        else if(pType.equals("Hoodies")){
                            nameList.add(GOTHoodiesName.get(pPosition));
                            priceList.add(GOTHoodiesPrice.get(pPosition));
                            imageList.add(GOTHoodiesImages.get(pPosition));
                            idList.add(GOTHoodiesId.get(pPosition));
                        }
                        else {
                            nameList.add(GOTTrousersName.get(pPosition));
                            priceList.add(GOTTrousersPrice.get(pPosition));
                            imageList.add(GOTTrousersImages.get(pPosition));
                            idList.add(GOTTrousersId.get(pPosition));
                        }

                    }
                    else if (pBrand.equals("Star Wars")){
                        if(pType.equals("Shirts")){
                            nameList.add(StarwarsShirtsName.get(pPosition));
                            priceList.add(StarwarsShirtsPrice.get(pPosition));
                            imageList.add(StarwarsShirtsImages.get(pPosition));
                            idList.add(StarwarsShirtsId.get(pPosition));
                        }
                        else if(pType.equals("Hoodies")){
                            nameList.add(StarwarsHoodiesName.get(pPosition));
                            priceList.add(StarwarsHoodiesPrice.get(pPosition));
                            imageList.add(StarwarsHoodiesImages.get(pPosition));
                            idList.add(StarwarsHoodiesId.get(pPosition));
                        }
                        else {
                            nameList.add(StarwarsTrousersName.get(pPosition));
                            priceList.add(StarwarsTrousersPrice.get(pPosition));
                            imageList.add(StarwarsTrousersImages.get(pPosition));
                            idList.add(StarwarsTrousersId.get(pPosition));
                        }
                    }
                    else if(pBrand.equals("Harry Potter")){
                        if(pType.equals("Shirts")){
                            nameList.add(HPShirtsName.get(pPosition));
                            priceList.add(HPShirtsPrice.get(pPosition));
                            imageList.add(HPShirtsImages.get(pPosition));
                            idList.add(HPShirtsId.get(pPosition));
                        }
                        else if(pType.equals("Hoodies")){
                            nameList.add(HPHoodiesName.get(pPosition));
                            priceList.add(HPHoodiesPrice.get(pPosition));
                            imageList.add(HPHoodiesImages.get(pPosition));
                            idList.add(HPHoodiesId.get(pPosition));
                        }
                        else {
                            nameList.add(HPTrousersName.get(pPosition));
                            priceList.add(HPTrousersPrice.get(pPosition));
                            imageList.add(HPTrousersImages.get(pPosition));
                            idList.add(HPTrousersId.get(pPosition));
                        }
                    }
                    else if(pBrand.equals("DC")){
                        if(pType.equals("Shirts")){
                            nameList.add(DCShirtsName.get(pPosition));
                            priceList.add(DCShirtsPrice.get(pPosition));
                            imageList.add(DCShirtsImages.get(pPosition));
                            idList.add(DCShirtsId.get(pPosition));
                        }
                        else if(pType.equals("Hoodies")){
                            nameList.add(DCHoodiesName.get(pPosition));
                            priceList.add(DCHoodiesPrice.get(pPosition));
                            imageList.add(DCHoodiesImages.get(pPosition));
                            idList.add(DCHoodiesId.get(pPosition));
                        }
                        else {
                            nameList.add(DCTrousersName.get(pPosition));
                            priceList.add(DCTrousersPrice.get(pPosition));
                            imageList.add(DCTrousersImages.get(pPosition));
                            idList.add(DCTrousersId.get(pPosition));
                        }
                    }
                    else if(pBrand.equals("Marvel")){
                        if(pType.equals("Shirts")){
                            nameList.add(MarvelShirtsName.get(pPosition));
                            priceList.add(MarvelShirtsPrice.get(pPosition));
                            imageList.add(MarvelShirtsImages.get(pPosition));
                            idList.add(MarvelShirtsId.get(pPosition));
                        }
                        else if(pType.equals("Hoodies")){
                            nameList.add(MarvelHoodiesName.get(pPosition));
                            priceList.add(MarvelHoodiesPrice.get(pPosition));
                            imageList.add(MarvelHoodiesImages.get(pPosition));
                            idList.add(MarvelHoodiesId.get(pPosition));
                        }
                        else {
                            nameList.add(MarvelTrousersName.get(pPosition));
                            priceList.add(MarvelTrousersPrice.get(pPosition));
                            imageList.add(MarvelTrousersImages.get(pPosition));
                            idList.add(MarvelTrousersId.get(pPosition));
                        }
                    }
                    else {
                        if(pType.equals("Shirts")){
                            nameList.add(OthersShirtsName.get(pPosition));
                            priceList.add(OthersShirtsPrice.get(pPosition));
                            imageList.add(OthersShirtsImages.get(pPosition));
                            idList.add(OthersShirtsId.get(pPosition));
                        }
                        else if(pType.equals("Hoodies")){
                            nameList.add(OthersHoodiesName.get(pPosition));
                            priceList.add(OthersHoodiesPrice.get(pPosition));
                            imageList.add(OthersHoodiesImages.get(pPosition));
                            idList.add(OthersHoodiesId.get(pPosition));
                        }
                        else {
                            nameList.add(OthersTrousersName.get(pPosition));
                            priceList.add(OthersTrousersPrice.get(pPosition));
                            imageList.add(OthersTrousersImages.get(pPosition));
                            idList.add(OthersTrousersId.get(pPosition));
                        }
                    }
                    total = total + (quantityList.get(i)*priceList.get(i));
                    i = i +1;




                }
                if(idList.isEmpty()){
                    Toast.makeText(CartActivity.this, "No Items in Cart", Toast.LENGTH_SHORT).show();
                    totalPrice.setText(Integer.toString(0));
                    TotalSend= totalPrice.getText().toString();
//                    cash.setVisibility(View.INVISIBLE);
//                    card.setVisibility(View.INVISIBLE);

                }
                else {
                    cartAdapter = new CartAdapter(CartActivity.this, sizeList, quantityList,
                            nameList, priceList, imageList, idList);
                    totalPrice.setText(Integer.toString(total));

                    TotalSend= totalPrice.getText().toString();
                    items=idList.size();

                    recyclerView.setAdapter(cartAdapter);

//                    cash.setVisibility(View.VISIBLE);
//                    card.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
