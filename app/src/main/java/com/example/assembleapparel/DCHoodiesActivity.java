package com.example.assembleapparel;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class DCHoodiesActivity extends AppCompatActivity {
    public static String userID;
    public RecyclerView recyclerView;
    public ArrayList<String> nameList;
    public ArrayList<Integer> priceList;
    public ArrayList<String> idList;
    public ArrayList<Integer> imageList;
    public String brand, type;


    RecyclerAdapter recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dchoodies);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        //SharedPreferences.Editor editor = pref.edit();
        userID = pref.getString("user_id", null); // getting String

        Intent get = getIntent();
        brand = get.getStringExtra("Brand");
        type = get.getStringExtra("Type");


        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        nameList = new ArrayList<>();
        priceList = new ArrayList<>();
        idList = new ArrayList<>();
        imageList = new ArrayList<>();
        setAdapter();

    }

    private void setAdapter() {
//        nameList.clear();
//        priceList.clear();
//        imageList.clear();
//        idList.clear();
//        recyclerView.removeAllViews();

        nameList.add("The Batman");
        nameList.add("Flash");
        nameList.add("The DC COMICS");
        nameList.add("Flash");



        priceList.add(1800);
        priceList.add(1500);
        priceList.add(1000);
        priceList.add(950);



        imageList.add(R.drawable.dc_hoodie_one);
        imageList.add(R.drawable.dc_hoodie_two);
        imageList.add(R.drawable.dc_hoodie_three);
        imageList.add(R.drawable.dc_hoodie_four);



        idList.add("42");
        idList.add("43");
        idList.add("44");
        idList.add("44");




        recyclerAdapter = new RecyclerAdapter(DCHoodiesActivity.this,
                nameList, priceList, imageList, idList, brand, type);
        recyclerView.setAdapter(recyclerAdapter);
    }
}
