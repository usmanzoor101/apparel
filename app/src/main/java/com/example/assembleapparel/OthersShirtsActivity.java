package com.example.assembleapparel;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class OthersShirtsActivity extends AppCompatActivity {
    public static String userID;
    public RecyclerView recyclerView;
    public ArrayList<String> nameList;
    public ArrayList<Integer> priceList;
    public ArrayList<String> idList;
    public ArrayList<Integer> imageList;

    RecyclerAdapter recyclerAdapter;

    public String brand, type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_others_shirts);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        //SharedPreferences.Editor editor = pref.edit();
        userID = pref.getString("user_id", null); // getting String

        Intent get = getIntent();
        brand = get.getStringExtra("Brand");
        type = get.getStringExtra("Type");


        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        nameList = new ArrayList<>();
        priceList = new ArrayList<>();
        idList = new ArrayList<>();
        imageList = new ArrayList<>();
        setAdapter();

    }

    private void setAdapter() {
//        nameList.clear();
//        priceList.clear();
//        imageList.clear();
//        idList.clear();
//        recyclerView.removeAllViews();

        nameList.add("Gym Tee");
        nameList.add("Skull");
        nameList.add("Mojo JOJO");
        nameList.add("Ninja Turtles");
        nameList.add("mojo JOJO");
        nameList.add("Design");





        priceList.add(800);
        priceList.add(500);
        priceList.add(1000);
        priceList.add(950);
        priceList.add(1000);
        priceList.add(350);





        imageList.add(R.drawable.other_one);
        imageList.add(R.drawable.other_two);
        imageList.add(R.drawable.other_three);
        imageList.add(R.drawable.other_four);
        imageList.add(R.drawable.other_five);
        imageList.add(R.drawable.other_six);





        idList.add("59");
        idList.add("60");
        idList.add("61");
        idList.add("62");
        idList.add("63");
        idList.add("64");





        recyclerAdapter = new RecyclerAdapter(OthersShirtsActivity.this,
                nameList, priceList, imageList, idList, brand, type);
        recyclerView.setAdapter(recyclerAdapter);
    }
}
