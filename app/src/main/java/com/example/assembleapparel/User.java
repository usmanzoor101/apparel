package com.example.assembleapparel;

public class User {
    public String userFname,userLname, userEmail, userPassword, userPhone, userAddress, userGender,userAge;
    public int cartSize;


    public User() {
    }

    public User(String userFname, String userLname, String userEmail, String userPhone, String userAddress,
                String userPassword, String userGender, String userAge, int cartSize){
        this.userFname = userFname;
        this.userLname = userLname;
        this.userPhone = userPhone;
        this.userEmail = userEmail;
        this.userPassword = userPassword;
        this.userAddress = userAddress;
        this.userGender = userGender;
        this.userAge = userAge;
        this.cartSize=cartSize;


    }


    public String userFname() {
        return userFname;
    }

    public void setuserFname(String userFname) {
        this.userFname = userFname;
    }

    public String userLname() {
        return userLname;
    }

    public void setuserLname(String userLname) {
        this.userLname = userLname;
    }


    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String user_email) {
        this.userEmail = user_email;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String user_password) {
        this.userPassword = user_password;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String user_phone) {
        this.userPhone = user_phone;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String userGender() {
        return userGender;
    }

    public void setuserGender(String userGender) {
        this.userGender = userGender;
    }

    public String userAge() {
        return userAge;
    }

    public void setuserAge(String userAge) {
        this.userAge = userAge;
    }

    public int cartSize() {
        return cartSize;
    }

    public void cartSize(int cartSize) {
        this.cartSize = cartSize;
    }
}

