package com.example.assembleapparel;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class PaymentActivity extends AppCompatActivity {

    public FirebaseUser firebaseUser;
    public DatabaseReference rUserDatabase;
    public FirebaseAuth mAuth;
    public static String userID;
    public SharedPreferences pref;
    public SharedPreferences.Editor editor;
    public Button cash, card;
    public EditText name,cnic, email,address;
    public TextView totalPrice,items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        Intent intent = getIntent();
        String total = intent.getStringExtra("TotalSend");
        String itemsGet = intent.getStringExtra("items");



        mAuth = FirebaseAuth.getInstance();
        FirebaseApp.initializeApp(this);
        rUserDatabase = FirebaseDatabase.getInstance().getReference();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        mAuth = FirebaseAuth.getInstance();

        totalPrice = findViewById(R.id.total);
        items=findViewById(R.id.items);
        name=findViewById(R.id.name);
        cnic=findViewById(R.id.cnic);
        address=findViewById(R.id.address);
        email=findViewById(R.id.email);
        cash=findViewById(R.id.cash);
        card=findViewById(R.id.card);

        totalPrice.setText(total);
        items.setText(itemsGet);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();
        userID = pref.getString("user_id", null); // getting String



        cash.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(TextUtils.isEmpty(name.getText().toString())|| TextUtils.isEmpty(email.getText().toString())
            || TextUtils.isEmpty(cnic.getText().toString())|| TextUtils.isEmpty(address.getText().toString())){
                Toast.makeText(PaymentActivity.this, "Please enter all information", Toast.LENGTH_SHORT).show();}
            else{
            Intent home = new Intent(PaymentActivity.this, DoneActivity.class);
            startActivity(home);
            finish();}
        }
    });

        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(name.getText().toString())|| TextUtils.isEmpty(email.getText().toString())
                        || TextUtils.isEmpty(cnic.getText().toString())|| TextUtils.isEmpty(address.getText().toString())){
                    Toast.makeText(PaymentActivity.this, "Please enter all information", Toast.LENGTH_SHORT).show();}
                else{
                Intent home_new = new Intent(PaymentActivity.this, DoneActivity.class);
                startActivity(home_new);
                finish();
            }
            }
        });

    emptyCart();
}

    private void emptyCart() {
        rUserDatabase= FirebaseDatabase.getInstance().getReference("User").child(userID);
        rUserDatabase.child("Products").removeValue();
    }
}
