package com.example.assembleapparel;

import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class HomeActivity extends AppCompatActivity {
    public Button got,starWars,harryPotter,Marvel,Dc,Others;
    public static String userID;
    public SharedPreferences pref;
    public SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);



        got = (Button)findViewById(R.id.gotButton);
        starWars = (Button)findViewById(R.id.starwarsButton);
        harryPotter = (Button)findViewById(R.id.harrypotterButton);
        Marvel = (Button)findViewById(R.id.marvelButton);
        Dc = (Button)findViewById(R.id.dcButton);
        Others = (Button)findViewById(R.id.othersButton);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();
        userID = pref.getString("user_id", null); // getting String


        got.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putString("brand_name", "GOT");
                FragmentManager fm = getFragmentManager();
                FragmentHome fragmentHome= new FragmentHome();
                fragmentHome.setArguments(args);
                fragmentHome.show(fm, "Sample Fragment");

            }
        });

        starWars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putString("brand_name", "Star Wars");
                FragmentManager fm = getFragmentManager();
                FragmentHome fragmentHome= new FragmentHome();
                fragmentHome.setArguments(args);
                fragmentHome.show(fm, "Sample Fragment");

            }
        });

        harryPotter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putString("brand_name", "Harry Potter");
                FragmentManager fm = getFragmentManager();
                FragmentHome fragmentHome= new FragmentHome();
                fragmentHome.setArguments(args);
                fragmentHome.show(fm, "Sample Fragment");

            }
        });

        Marvel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putString("brand_name", "Marvel");
                FragmentManager fm = getFragmentManager();
                FragmentHome fragmentHome= new FragmentHome();
                fragmentHome.setArguments(args);
                fragmentHome.show(fm, "Sample Fragment");

            }
        });

        Dc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putString("brand_name", "DC");
                FragmentManager fm = getFragmentManager();
                FragmentHome fragmentHome= new FragmentHome();
                fragmentHome.setArguments(args);
                fragmentHome.show(fm, "Sample Fragment");

            }
        });

        Others.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putString("brand_name", "Other");
                FragmentManager fm = getFragmentManager();
                FragmentHome fragmentHome= new FragmentHome();
                fragmentHome.setArguments(args);
                fragmentHome.show(fm, "Sample Fragment");

            }
        });


    }
//Menu items options
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }
    //Menu items
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.cart:
                Intent cart = new Intent(HomeActivity.this, CartActivity.class);
                startActivity(cart);
                //cart page
                return true;

            case R.id.profile:
                Intent update = new Intent(HomeActivity.this, ProfileActivity.class);
                startActivity(update);
                return true;

            case R.id.maps:
//maps store activity
                Intent maps = new Intent(HomeActivity.this, MapsActivity.class);
                startActivity(maps);
                return true;

            case R.id.weather:
//weather Check activity
                Intent weather = new Intent(HomeActivity.this, WeatherActivity.class);
                startActivity(weather);
                return true;


            case R.id.logout:
//end preference and main login screen
                editor.clear();
                editor.commit(); // commit changes
                Intent logout = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(logout);
                return true;

            default:
                //if no option choosen from menu
                return super.onOptionsItemSelected(item);
        }
    }

}
